
var timer = null;

function showTime() {
   $("#text")[0].innerHTML = Date();
   //  document.getElementById("text").innerHTML = Date();
}

function start() {
    if (timer === null) {
        timer = setInterval(showTime, 500);
    }
}

function stop() {
    if (timer !== null) {
        clearInterval(timer);
        timer = null;
    }
}

$(function () {
    $("#startBtn").on("click", start);
    $("#stopBtn").on("click", stop);

    $("#stopBtn").mouseover(function () {
        this.innerHTML = "Hover";
        this.style.fontWeight = "bold";
    });


})